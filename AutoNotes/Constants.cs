﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoNotes
{
    internal static class Constants
    {
        public const string SummaryNotes = "SummaryNotes";
        public const string OriginalNotes = "OriginalNotes";
        public const string company = "company";
        public const string Name = "Name";
        public const string TimeFormate = "TimeFormate";
    }
}
