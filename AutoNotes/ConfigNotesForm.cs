﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoNotes
{
    public partial class ConfigNotesForm : Form
    {
        public ConfigNotesForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetConfig();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SetConfig();
            this.Close();
        }

        private void SetConfig()
        {
            if (!string.IsNullOrEmpty(this.richTextBox1.Text))
                ConfigSettings.Default[Constants.SummaryNotes] = this.richTextBox1.Text;
            if (!string.IsNullOrEmpty(this.richTextBox2.Text))
                ConfigSettings.Default[Constants.OriginalNotes] = this.richTextBox2.Text;
            if (!string.IsNullOrEmpty(this.textBox1.Text))
                ConfigSettings.Default[Constants.company] = this.textBox1.Text;
            if (!string.IsNullOrEmpty(this.textBox2.Text))
                ConfigSettings.Default[Constants.Name] = this.textBox2.Text;
            if (!string.IsNullOrEmpty(this.textBox3.Text))
                ConfigSettings.Default[Constants.TimeFormate] = this.textBox3.Text;
            ConfigSettings.Default.Save();
        }

        private void LoadConfig()
        {
            richTextBox1.Text = ConfigSettings.Default[Constants.SummaryNotes].ToString();
            richTextBox2.Text = ConfigSettings.Default[Constants.OriginalNotes].ToString();
            textBox1.Text = ConfigSettings.Default[Constants.company].ToString();
            textBox2.Text = ConfigSettings.Default[Constants.Name].ToString();
            textBox3.Text = ConfigSettings.Default[Constants.TimeFormate].ToString();
        }

        private void ConfigNotesForm_Load(object sender, EventArgs e)
        {
            LoadConfig();
        }
    }
}
